import 'react-native-gesture-handler';
import React from 'react';
import {Text, TouchableOpacity, Image, View} from 'react-native';

import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import SplashScreen from './screens/SplashScreen';
import HomeScreen from './screens/HomeScreen';
import LoginScreen from './screens/LoginScreen';
import RegisterScreen from './screens/RegisterScreen';
import MenuScreen from './screens/MenuScreen';
import ProfileScreen from './screens/ProfileScreen';
import ProfileSurpriseScreen from './screens/ProfileSurpriseScreen';
import ProfileNameScreen from './screens/ProfileNameScreen';
import StoreScreen from './screens/StoreScreen';
import StoreCartScreen from './screens/StoreCartScreen';
import MapsScreen from './screens/MapsScreen';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const Tab = createBottomTabNavigator();
function MyTabs() {
  return (
    <Tab.Navigator initialRouteName={'SplashScreen'}>
      <Tab.Screen
        name={'SplashScreen'}
        component={SplashScreen}
        options={{headerShown: false}}
      />
      <Tab.Screen
        name={'HomeScreen'}
        component={HomeScreen}
        options={{headerShown: false}}
      />
      <Tab.Screen
        name={'MenuScreen'}
        component={MenuScreen}
        options={{headerShown: false}}
      />
      <Tab.Screen
        name={'MapsScreen'}
        component={MapsScreen}
        options={{headerShown: false}}
      />
      <Tab.Screen
        name={'ProfileScreen'}
        component={ProfileScreen}
        options={{headerShown: false}}
      />
      <Tab.Screen
        name="Perfil"
        component={ProfileNameScreen}
        options={
          ({
            tabBarLabel: 'Profile',
            tabBarIcon: ({color, size}) => (
              <Icon name="account" color={color} size={size} />
            ),
          },
          {headerShown: false})
        }
      />
    </Tab.Navigator>
  );
}

const Stack = createStackNavigator();

function StackLoggedOutUser() {
  return (
    <Stack.Navigator initialRouteName={'SplashScreen'}>
      <Stack.Screen
        name={'SplashScreen'}
        component={SplashScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name={'HomeScreen'}
        component={HomeScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name={'MenuScreen'}
        component={MenuScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name={'ProfileScreen'}
        component={ProfileScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name={'ProfileSurpriseScreen'}
        component={ProfileSurpriseScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name={'ProfileNameScreen'}
        component={ProfileNameScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name={'StoreScreen'}
        component={StoreScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name={'StoreCartScreen'}
        component={StoreCartScreen}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name={'MapsScreen'}
        component={MapsScreen}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
}

function StackLoggedInUser({navigation}) {
  return (
    <Stack.Navigator
      initialRouteName={'MenuScreen'}
      screenOptions={{
        headerTitleAlign: 'center',
        headerStyle: {
          backgroundColor: '#f6f6f6',
        },
        headerTintColor: '#999999',
        headerTitleStyle: {
          fontWeight: '500',
        },
      }}>
      <Stack.Screen name={'MenuScreen'} component={MenuScreen} />
      <Stack.Screen name={'ProfileScreen'} component={ProfileScreen} />
    </Stack.Navigator>
  );
}

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName={'MyTabs'}>
        <Stack.Screen
          name={'StackLoggedOut'}
          component={StackLoggedOutUser}
          options={{headerShown: false}}
        />
        <Stack.Screen
          name={'StackLoggedIn'}
          component={StackLoggedInUser}
          options={{headerShown: false}}
        />
        {/* <Tab.Screen
          name={'Home'}
          component={HomeScreen}
          options={{headerShown: false}}
        />
        <Tab.Screen
          name={'Perfil'}
          component={StackLoggedOutUser}
          options={
            ({
              tabBarLabel: 'Profile',
              tabBarIcon: ({color, size}) => (
                <Icon name="account" color={color} size={size} />
              ),
            },
            {headerShown: false})
          }
        /> */}
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
