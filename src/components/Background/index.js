import LinearGradient from 'react-native-linear-gradient';
import styled from 'styled-components/native';

export default styled(LinearGradient).attrs({
  colors: ['#f62328', '#ffffff'],
})`
  flex: 1;
`;
