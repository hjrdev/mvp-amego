import React from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';

import Profile from '../assets/images/profile03-name.png';
import Header from '../assets/images/profile03-name-top.png';
import Sale from '../assets/images/profile03-name-sale.png';
import Name from '../assets/images/profile03-name-name.png';

import Missoes from '../assets/images/profile03-name-missoes.png';
import Maps from '../assets/images/profile03-name-maps.png';
import Qrcode from '../assets/images/profile03-name-qrcode.png';

export default function ProfileNameScreen({navigation}) {
  return (
    <View style={styles.container}>
      <View style={styles.containerImage}>
        <Image
          source={Profile}
          resizeMode="cover"
          style={styles.backgroundImage}
        />
      </View>

      <View style={styles.containerHeader}>
        <Image source={Header} resizeMode="cover" style={styles.headerImage} />
        <View style={styles.containerName}>
          <Image source={Name} resizeMode="contain" style={styles.nameImage} />
        </View>

        <View style={styles.containerIcons}>
          <Image
            source={Missoes}
            resizeMode="contain"
            style={styles.imageIco}
          />
          <TouchableOpacity onPress={() => navigation.navigate('MapsScreen')}>
            <Image source={Maps} resizeMode="contain" style={styles.imageIco} />
          </TouchableOpacity>

          <Image source={Qrcode} resizeMode="contain" style={styles.imageIco} />
        </View>
      </View>

      <View style={styles.containerButton}>
        <TouchableOpacity onPress={() => navigation.navigate('MenuScreen')}>
          <Image source={Sale} resizeMode="cover" style={styles.button} />
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  containerHeader: {
    flex: 1,
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingVertical: 5,
  },
  headerImage: {
    height: 100,
    position: 'relative',
    alignItems: 'center',
    paddingHorizontal: 10,
  },
  containerName: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerIcons: {
    marginTop: 50,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(255, 255, 255, 0.7)',
    paddingVertical: 50,
    borderRadius: 10,
  },
  imageIco: {width: 90, paddingLeft: 30},
  nameImage: {width: 120, height: 90},
  containerImage: {
    display: 'flex',
    flex: 1,
    position: 'absolute',
  },
  backgroundImage: {
    height: Dimensions.get('window').height,
    width: Dimensions.get('window').width,
  },
  containerButton: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
  button: {
    width: 100,
    position: 'relative',
  },
});
