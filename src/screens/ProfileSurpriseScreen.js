import React from 'react';
import {
  View,
  StyleSheet,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';

import Profile from '../assets/images/profile02-surprise.jpeg';
import ButtonPlay from '../assets/images/home-play.png';

export default function ProfileSurpriseScreen({navigation}) {
  return (
    <View style={styles.container}>
      <View style={styles.containerImage}>
        <Image
          source={Profile}
          resizeMode="cover"
          style={styles.backgroundImage}
        />
      </View>

      <View style={styles.containerButton}>
        <TouchableOpacity
          onPress={() => navigation.navigate('ProfileNameScreen')}>
          <Image source={ButtonPlay} resizeMode="cover" style={styles.button} />
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  containerImage: {
    display: 'flex',
    flex: 1,
    position: 'absolute',
  },
  backgroundImage: {
    height: Dimensions.get('window').height,
    width: Dimensions.get('window').width,
  },
  containerButton: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
  button: {
    width: 100,
    position: 'relative',
  },
});
