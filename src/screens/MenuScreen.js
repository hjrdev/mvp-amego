import React from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  Share,
  Alert,
} from 'react-native';

import Profile from '../assets/images/menu-profile.png';
import Hank from '../assets/images/menu-hanking.png';
import Friends from '../assets/images/menu-friends.png';
import Store from '../assets/images/menu-store.png';
import Settings from '../assets/images/menu-settings.png';
import History from '../assets/images/menu-history.png';
import Logout from '../assets/images/menu-logout.png';
import Play from '../assets/images/home-play.png';

export default function MenuScreen({navigation}) {
  const onShare = async () => {
    try {
      const result = await Share.share({
        message: 'Chame seus amigos para ganhas muitos prêmios',
      });
    } catch (error) {
      Alert.alert(
        'Algo não deu certo...',
        'Você não tem nenhuma rede social instalada.',
      );
    }
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={styles.containerListItem}
        onPress={() => navigation.navigate('ProfileNameScreen')}>
        <View style={styles.containerItems}>
          <Image source={Profile} resizeMode="contain" style={styles.icons} />
          <Text style={styles.title}>Perfil</Text>
        </View>
        <Image source={Play} resizeMode="contain" style={styles.buttonPlay} />
      </TouchableOpacity>

      <TouchableOpacity style={styles.containerListItem} onPress={() => {}}>
        <View style={styles.containerItems}>
          <Image source={Hank} resizeMode="contain" style={styles.icons} />
          <Text style={styles.title}>Hanking</Text>
        </View>
        <Image source={Play} resizeMode="contain" style={styles.buttonPlay} />
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.containerListItem}
        onPress={() => navigation.navigate('StoreScreen')}>
        <View style={styles.containerItems}>
          <Image source={Store} resizeMode="contain" style={styles.icons} />
          <Text style={styles.title}>Loja</Text>
        </View>
        <Image source={Play} resizeMode="contain" style={styles.buttonPlay} />
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.containerListItem}
        onPress={() => onShare()}>
        <View style={styles.containerItems}>
          <Image source={Friends} resizeMode="contain" style={styles.icons} />
          <Text style={styles.title}>Amigos</Text>
        </View>
        <Image source={Play} resizeMode="contain" style={styles.buttonPlay} />
      </TouchableOpacity>

      <TouchableOpacity style={styles.containerListItem}>
        <View style={styles.containerItems}>
          <Image source={History} resizeMode="contain" style={styles.icons} />
          <Text style={styles.title}>História</Text>
        </View>
        <Image source={Play} resizeMode="contain" style={styles.buttonPlay} />
      </TouchableOpacity>

      <TouchableOpacity style={styles.containerListItem}>
        <View style={styles.containerItems}>
          <Image source={Settings} resizeMode="contain" style={styles.icons} />
          <Text style={styles.title}>Configurações</Text>
        </View>
        <Image source={Play} resizeMode="contain" style={styles.buttonPlay} />
      </TouchableOpacity>

      <TouchableOpacity style={styles.containerListItem}>
        <View style={styles.containerItems}>
          <Image source={Logout} resizeMode="contain" style={styles.icons} />
          <Text style={styles.title}>Sair</Text>
        </View>
        <Image source={Play} resizeMode="contain" style={styles.buttonPlay} />
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: '#222',
    paddingHorizontal: 20,
  },
  containerListItem: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottomWidth: 2,
    borderBottomColor: '#444444',
    height: 80,
  },
  containerItems: {
    flexDirection: 'row',
    alignItems: 'center',
    textAlign: 'left',
    width: '85%',
  },
  icons: {
    width: 40,
    paddingLeft: 20,
  },
  buttonPlay: {
    alignItems: 'center',
    width: 50,
    height: 50,
    marginTop: 14,
  },
  title: {
    fontSize: 22,
    color: '#ffffff',
    textAlign: 'left',
    marginLeft: 30,
  },
});
