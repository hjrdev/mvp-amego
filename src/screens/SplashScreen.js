import React, {useEffect} from 'react';
import {Text, View, StyleSheet, StatusBar, Image} from 'react-native';

import Background from '../components/Background';

import Logo from '../assets/images/splashscreen.jpeg';

export default function SplashScreen({navigation}) {
  useEffect(() => {
    setTimeout(() => {
      navigation.navigate('HomeScreen');
    }, 2000);
  }, [navigation]);

  return (
    <Background style={styles.container}>
      <StatusBar BackgroundStyle="light" backgroundColor="#f62328" />
      <Image source={Logo} resizeMode="cover" style={styles.logo} />
    </Background>
  );
}

const styles = StyleSheet.create({
  container: {flex: 1, alignItems: 'center', justifyContent: 'center'},
  logo: {width: '100%'},
});
